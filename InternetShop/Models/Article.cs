﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace InternetShop.Models
{
    public class Article
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public int Available { get; set; }
        public bool Discounted { get; set; }
        public float DiscountedPrice { get; set; }
    }
}
